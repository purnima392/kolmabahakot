<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db_tamusamaj');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Lxb/1HRpuZf_AB%B hCEiEvp|MaH~gg!B8EuL89Fmc@Z.12z+(L.UnVX6cr[ImnR');
define('SECURE_AUTH_KEY',  'hD}L`JC<%n3B&cXEP2t}e[oR,L/MqwZtfX:0csEm_uE*N/-$3Es60gEDz3G)|t,q');
define('LOGGED_IN_KEY',    '9#|J#,4D]l#siYOsTq:U|<4[b$Cfn*6PC^r!4fmGi~N)>3YbzOFgsUS#z$m^59,c');
define('NONCE_KEY',        'x.WwE,@n}Cb^,iw@Mz( n:nk&g?m|n9?!I^G5QKM+a?vks(x%VaVh.HK+p3FTHJ=');
define('AUTH_SALT',        'yqb<gUK$,tSQ6z:J5_+_4G@!&HsBw*k_>d@NHV@;F-Qh}W.4lq~k:^.<%9C4}@XQ');
define('SECURE_AUTH_SALT', 'u})5?R4S7 Z0MkP}Rh{+`wYg<%uwaoYEQ~^DO  gR=u:R~v)9ecwSUj?C18Ndlg?');
define('LOGGED_IN_SALT',   'y1d:H3zy~GHG%,9f$ U;9D~&8jTJ92P[4/A[yqhj(.^0C^clND;C:47tsK`4<vu%');
define('NONCE_SALT',       '~3!RSFLW$lgOSoJT2u@SZ%Y,68![)[!))4o]x Sq[g$@g@;c<AP6sR,Xfe}HQi=s');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'kb_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
