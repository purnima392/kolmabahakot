<?php 
/*Template Name: Home*/

get_header();?>
<!--Slider Start-->
<div class="slider-wrapper">
  <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
   <?php  $args = array(
    'post_type'     => 'slider',
    'posts_per_page'=>5
  );
   $slider = new WP_Query($args);
			 //$slider=get_posts(array('post_type'=>'book','posts_per_page' => 5));

   if($slider->have_posts()){
    $count = $slider->found_posts;
  }
  ?>
  <div class="carousel-inner">

    <?php $i = 0; 
    while($slider->have_posts()): $slider->the_post(); ?>
    <div class="carousel-item   
    <?php echo ($i == 0) ? 'active' : '' ;?>">
    <?php 
    the_post_thumbnail( 'slider', array('class' => 'd-block img-fluid','alt' => get_the_title() ) ) ; ?>

    <div class="carousel-caption d-none d-md-block">
      <?php $Title =get_the_title();
      $Content= get_the_content(); ?>
      <h3><?=$Title?></h3>
      <?=$Content?>
      <a href="#" class="btn btn-outline">Start Exploring</a>
    </div>
  </div>

  <?php 
  $i++; 
endwhile; 
?>
</div>
<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> 
</div>
</div>
<!--Slider End--> 
<!-- Activities Start -->
<section class="activities">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6">
       <?php $the_query = new WP_Query( 'page_id=82'  ); ?>
       <?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
         <div class="act_item"> <i class="act_icon icon icon-Users"></i>
          <div class="act_content">
            <h3><a href="<?php echo get_permalink('82')?>"><?php the_title();?></a></h3>
            <p><?php the_content();?></p>
            <a href="<?php echo get_permalink('82')?>" class="links">Join Now <i class="fa fa-long-arrow-right"></i></a></div>
          </div>
        <?php endwhile;?>
      </div>
      <div class="col-lg-6 col-md-6">
        <?php $the_query = new WP_Query( 'page_id=39'  ); ?>
        <?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
          <div class="act_item donate-wrapper"> <i class="act_icon icon icon-Dollars"></i>
            <div class="act_content">
              <h3><a href="<?php echo get_permalink('39')?>"><?php the_title();?></a></h3>
              <p><?php the_content();?></p>
              <a href="<?php echo get_permalink('39')?>" class="links">Donate Here<i class="fa fa-long-arrow-right"></i></a></div>
            </div>
          <?php endwhile;?>
        </div>
      </div>
    </div>
  </section>
  <!-- Activities End --> 
  <!-- Content Start -->
  <section class="content">
    <div class="container">
     <?php $the_query = new WP_Query( 'page_id=17'  ); ?>
     <?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
      <div class="row v-align-children">
        <div class="col-lg-5 col-md-6">
          <div class="welcome_txt">
            <h2><?php the_excerpt();?></h2>
            <p><?php the_content();?></p>
            <a href="<?php echo get_permalink('17')?>" class="btn">Read More</a> </div>
          </div>
          <div class="col-lg-7 col-md-6">
            <div class="primary-box">
              <?php if (has_post_thumbnail( $post->ID ) ): ?>
                <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                <img class="primary-img" src="<?php echo $image[0]; ?>" alt="<?php the_title();?>">
              <?php endif; ?>
            <?php endwhile;?>           
          </div>    
        </div>
      </div>
      <div class="advertise">
        <?php  
        $args = array(
          'post_type' => 'advertise',
          'meta_query' => array(      
            'key' => 'prfx-textdomain',
            'value' => 'yes',
            'compare' => '='
          ),        
        );
        $advertise = new WP_Query($args);
       //$slider=get_posts(array('post_type'=>'book','posts_per_page' => 5));

        if($advertise->have_posts()){
          $count = $advertise->found_posts;
        }
        ?>

        <?php
        while($advertise->have_posts()): $advertise->the_post(); ?>
        <?php if (has_post_thumbnail( $post->ID ) ): ?>
          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
          <img src="<?php echo $image[0]; ?>" alt="<?php the_title();?>">
        <?php endif; 
      endwhile; 
      ?>
    </div>                            
  </div>
</section>
<!-- Content End --> 
<!-- Project Start -->
<section class="content pt-0">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-md-4 history">
       <?php $the_query = new WP_Query( 'page_id=25'  ); ?>
       <?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
        <div class="title">
          <h3><?php the_title();?></h3>
        </div>
        <p><?php the_content();?></p>
        <a href="<?php echo get_permalink('25')?>" class="btn">Read More</a>
      <?php endwhile;?> 
    </div>
    <div class="col-lg-4 col-md-4">
      <div class="advertise">
       <?php  
       $args = array(
        'post_type' => 'advertise',
        'meta_query' => array(      
          'key' => 'prfx-textdomain',
          'value' => 'yes',
          'compare' => '='
        ),        
      );
       $advertise = new WP_Query($args);
       //$slider=get_posts(array('post_type'=>'book','posts_per_page' => 5));

       if($advertise->have_posts()){
        $count = $advertise->found_posts;
      }
      ?>

      <?php
      while($advertise->have_posts()): $advertise->the_post(); ?>
      <?php if (has_post_thumbnail( $post->ID ) ): ?>
        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
        <img src="<?php bloginfo('template_url')?>/img/bb14a5b2177d1f765013b9c415706c5b--do-you-need-like-you.jpg" alt="<?php the_title();?>">
      <?php endif; 
    endwhile; 
    ?>

  </div>


</div>
<div class="col-lg-4 col-md-4">
  <div class="right-sidebar">
    <div class="side_block">
      <h4><i class="fa fa-calendar"></i>Latest Events</h4>
      <?php $args = array(
        'posts_per_page'   => 5,
        'post_type'        => 'post',
      );
      $posts_array = get_posts( $args ); ?>
      <?php $the_query = new WP_Query( $args); ?>
      <?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
        <div class="event_item">
          <?php if (has_post_thumbnail( $post->ID ) ): ?>
            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>


            <span class="event_img"><img src="<?php echo $image[0]; ?>" alt="<?php the_title();?>" ></span>
          <?php endif; ?>
          <div class="event_detail">
            <h5><a href="#"><?php the_title();?></a></h5>
            <span class="date"><?php echo get_the_date(); ?></span>
            <p><?php the_content();?></p>
          </div>
        </div>
      <?php endwhile;?> 

    </div>
  </div>
</div>
</div>
<div class="advertise">
  <img src="<?php bloginfo('template_url')?>/img/x999-168-1000-187-0-0-581-advertise-banner.png.pagespeed.ic.kG_DDlR2Yg.png" alt="">
</div>  
</div>
</section>
<?php get_footer();?>