<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Kolma Bahakot Tamu Samaj</title>
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!--Icon Fonts-->
  <link href="<?php bloginfo('template_url')?>/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  
  <?php wp_head();?>
</head>
<body>
	<!-- scrollToTop --> 
  <!-- ================ -->
  <div class="scrollToTop"><i class="fa fa-angle-up"></i></div>
  <!--Header Start-->
  <header>

    <nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top" role="navigation">
       <a class="navbar-brand" href="index.php">Kolma Bahakot <span>Tamu Samaj</span></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
        <?php
            wp_nav_menu(array(
                'menu'            => 'primary',
                'theme_location'  => 'primary',
                'container'       => 'div',
                'container_id'    => 'bs4navbar',
                'container_class' => 'collapse navbar-collapse',
                'menu_id'         => false,
                'menu_class'      => 'navbar-nav ml-auto',
                'depth'           => 2,
                'fallback_cb'     => 'bs4navwalker::fallback',
                'walker'          => new bs4navwalker()
              ));
        ?>
      
    </nav>
  </header>