<?php
	
	wp_enqueue_style('animate',get_template_directory_uri().'/css/animate.css',false,'all');
	wp_enqueue_style('animations',get_template_directory_uri().'/css/animations.css',false,'all');
	wp_enqueue_style('bootstrap',get_template_directory_uri().'/css/bootstrap.min.css',false,'all');
	wp_enqueue_style('magnific-popup',get_template_directory_uri().'/css/magnific-popup.css',false,'all');
	wp_enqueue_style('magnific-popup',get_template_directory_uri().'/css/magnific-popup.css',false,'all');
	wp_enqueue_style('stroke-gap-icon',get_template_directory_uri().'/css/stroke-gap-icon.css',false,'all');
	wp_enqueue_style('unite-gallery',get_template_directory_uri().'/css/unite-gallery.css',false,'all');
	wp_enqueue_style('style',get_template_directory_uri().'/css/style.css',false,'all');
	wp_enqueue_script('jquery.min',get_template_directory_uri().'/js/jquery.min.js',true,'all');
	wp_enqueue_script('bootstrap.min',get_template_directory_uri().'/js/bootstrap.min.js',true,'all');
	wp_enqueue_script('popper.min',get_template_directory_uri().'/js/popper.min.js',true,'all');
	wp_enqueue_script('holder.min',get_template_directory_uri().'/js/holder.min.js',true,'all');
	wp_enqueue_script('unitegallery.min',get_template_directory_uri().'/js/unitegallery.min.js',true,'all');
	wp_enqueue_script('ug-theme-tiles',get_template_directory_uri().'/js/ug-theme-tiles.js',true,'all');
	wp_enqueue_script('jquery.magnific-popup.min',get_template_directory_uri().'/js/jquery.magnific-popup.min.js',true,'all');
	wp_enqueue_script('script',get_template_directory_uri().'/js/script.js',true,'all');
	
    //Register Now Walker class_alias
    require( get_template_directory().'/bs4navwalker.php');
    //Theme Support
    function register_my_menu() {
		register_nav_menu('primary',__( 'Primary Menu' ));
	}
	add_action( 'init', 'register_my_menu' );
	//bootstrap carasouel
	
	/**
		* Register a Custom post type for.
	*/
	add_theme_support( 'post-thumbnails' );

  function codex_custom_init() {
  $labels = array(
    'name' => _x('Slider', 'post type general name'),
    'singular_name' => _x('Slider', 'post type singular name'),
    'add_new' => _x('Add New', 'slider'),
    'add_new_item' => __('Add New slider'),
    'edit_item' => __('Edit slider'),
    'new_item' => __('New slider'),
    'all_items' => __('All slider'),
    'view_item' => __('View slider'),
    'search_items' => __('Search slider'),
    'not_found' =>  __('No slider found'),
    'not_found_in_trash' => __('No slider found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => __('Slider')

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array( 'title','editor', 'author', 'thumbnail', 'excerpt', 'comments' )
  ); 
  register_post_type('Slider',$args);
}
add_action( 'init', 'codex_custom_init' );
//For Advertise
function codex_advertise_init() {
  $labels = array(
    'name' => _x('Advertise', 'post type general name'),
    'singular_name' => _x('Advertise', 'post type singular name'),
    'add_new' => _x('Add New', 'advertise'),
    'add_new_item' => __('Add New advertise'),
    'edit_item' => __('Edit advertise'),
    'new_item' => __('New advertise'),
    'all_items' => __('All advertise'),
    'view_item' => __('View advertise'),
    'search_items' => __('Search advertise'),
    'not_found' =>  __('No advertise found'),
    'not_found_in_trash' => __('No advertise found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => __('Advertise')

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array( 'title', 'author', 'thumbnail', 'excerpt', 'comments' )
  ); 
  register_post_type('Advertise',$args);
}
add_action( 'init', 'codex_advertise_init' );
// To remove unwanted p tag
remove_filter ('the_content', 'wpautop');
remove_filter ('the_excerpt', 'wpautop');
// For Excerpt
function wpcodex_add_excerpt_support_for_pages() {
  add_post_type_support( 'page', 'excerpt' );
}
add_action( 'init', 'wpcodex_add_excerpt_support_for_pages' );
/**
 * Adds a meta box to the post editing screen
 */
function prfx_custom_meta() {
  add_meta_box( 'prfx_meta', __( 'Meta Box Title', 'prfx-textdomain' ), 'prfx_meta_callback', 'advertise' );
}
add_action( 'add_meta_boxes', 'prfx_custom_meta' );

/**
 * Outputs the content of the meta box
 */
function prfx_meta_callback( $post ) {
  wp_nonce_field( basename( __FILE__ ), 'prfx_nonce' );
  $prfx_stored_meta = get_post_meta( $post->ID );
  ?>

  <p>
    <span class="prfx-row-title"><?php _e( 'Category', 'prfx-textdomain' )?></span>
    <div class="prfx-row-content">
      <label for="meta-checkbox">
        <input type="checkbox" name="meta-checkbox" id="meta-checkbox" value="yes" <?php if ( isset ( $prfx_stored_meta['meta-checkbox'] ) ) checked( $prfx_stored_meta['meta-checkbox'][0], 'yes' ); ?> />
        <?php _e( 'Checkbox label', 'prfx-textdomain' )?>
      </label>
      <label for="meta-checkbox-two">
        <input type="checkbox" name="meta-checkbox-two" id="meta-checkbox-two" value="yes" <?php if ( isset ( $prfx_stored_meta['meta-checkbox-two'] ) ) checked( $prfx_stored_meta['meta-checkbox-two'][0], 'yes' ); ?> />
        <?php _e( 'Another checkbox', 'prfx-textdomain' )?>
      </label>
    </div>
  </p>
<?php
}
/**
 * Saves the custom meta input
 */
function prfx_meta_save( $post_id ) {
 
  // Checks save status
  $is_autosave = wp_is_post_autosave( $post_id );
  $is_revision = wp_is_post_revision( $post_id );
  $is_valid_nonce = ( isset( $_POST[ 'prfx_nonce' ] ) && wp_verify_nonce( $_POST[ 'prfx_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
  // Exits script depending on save status
  if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
    return;
  }
 
  

  // Checks for input and saves
  if( isset( $_POST[ 'meta-checkbox' ] ) ) {
    update_post_meta( $post_id, 'meta-checkbox', 'yes' );
  } else {
    update_post_meta( $post_id, 'meta-checkbox', '' );
  }
   
  // Checks for input and saves
  if( isset( $_POST[ 'meta-checkbox-two' ] ) ) {
    update_post_meta( $post_id, 'meta-checkbox-two', 'yes' );
  } else {
    update_post_meta( $post_id, 'meta-checkbox-two', '' );
  }


 
}
add_action( 'save_post', 'prfx_meta_save' );

?>