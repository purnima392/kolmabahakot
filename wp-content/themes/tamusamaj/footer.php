<footer class="footer">
                                      <div class="container">
                                        <div class="row">
                                          <div class="col-md-3 col-sm-3">
                                            <?php $the_query = new WP_Query( 'page_id=17'  ); ?>
                                              <?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
                                            <div class="footer-content">
                                              <div class="footer-logo">
                                                <h4><?php the_title();?></h4>
                                                <p><?php the_content();?></p>
                                                <a href="<?php echo get_permalink('17')?>" class="links">Read More</a> </div>
                                              </div>
                                              <?php endwhile;?>
                                            </div>
                                            <div class="col-md-9 col-sm-9">
                                              <div class="row">
                                                <div class="col-md-4 col-sm-4">
                                                  <h4>Connect</h4>
                                                  <ul>
                                                    <li><a href="#">+977-532305</a></li>
                                                    <li><a href="#">info@kolmabahakotpokhara.org</a></li>
                                                    <li>Pokhara Lekhnath -10, Indra Marga, Kaski, Nepal</li>
                                                  </ul>
                                                </div>
                                                <div class="col-md-3 col-sm-3">
                                                  <h4>Social Media</h4>
                                                  <ul class="social-links">
                                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                  </ul>
                                                </div>
                                                <div class="col-md-5 col-sm-5">
                                                  <h4>Subscribe to our newsletter</h4>
                                                  <small>We will send you updates on how our team working on new projects and general news.</small>
                                                  <div class="newsletter">
                                                    <form>
                                                      <div class="form-group">
                                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                                      </div>
                                                      <button type="submit" class="btn btn-primary">Subscribe</button>
                                                    </form>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="bottom-footer text-center ">
                                          <div class="container clearfix">
                                            <p class="copyright float-left">© 2017 Kolma Bahakot Tamu Samaj. All rights reserved</p>
                                            <p class="float-right">Design by <a href="#">Webpage Nepal</a></p>
                                          </div>
                                        </div>
                                      </footer>
                                      
                                      <?php wp_footer();?>
                                    </body>
                                    </html>