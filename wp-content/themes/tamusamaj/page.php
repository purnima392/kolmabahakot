<?php get_header();?>
<section class="content inner-content">
	<div class="container">
		<?php
  if (have_posts()) : while (have_posts()) : the_post();
  ?> 
<h2 class="innertitle"><?php the_title();?></h2>
<div class="circle-img"><?php if (has_post_thumbnail( $post->ID ) ): ?>
                <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                <img class="primary-img" src="<?php echo $image[0]; ?>" alt="<?php the_title();?>">
              <?php endif; ?>
              </div>
<?php the_content();?>

  <?php endwhile; 
endif; ?> 

	</div>
</section>
<?php get_footer();?>